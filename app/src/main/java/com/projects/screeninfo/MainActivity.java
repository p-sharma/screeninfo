package com.projects.screeninfo;

import android.app.ListActivity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ListActivity {

    private static final String TAG = MainActivity.class.getName();

    private InfoListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new InfoListAdapter(this, R.layout.row_layout, new ArrayList<Info>());
        setListAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDisplayMetricsData();
        getCpuInfo();
        getMemoryInfo();
    }

    private void loadDisplayMetricsData() {
        DisplayMetrics dm = getResources().getDisplayMetrics();
        List<Info> infoListItems = adapter.getInfoList();

        /**********************************************/
        /*Info density = new Info();
        density.paramName = "density";
        density.paramValue = String.valueOf(dm.density);
        infoListItems.add(density);*/

        /**********************************************/

        /*Info scaledDensity = new Info();
        scaledDensity.paramName = "scaledDensity";
        scaledDensity.paramValue = String.valueOf(dm.scaledDensity);
        infoListItems.add(scaledDensity);*/

        /**********************************************/

        /*Info densityDpi = new Info();
        densityDpi.paramName = "densityDpi";
        densityDpi.paramValue = String.valueOf(dm.densityDpi);
        infoListItems.add(densityDpi);*/

        /**********************************************/

        /*Info heightPixels = new Info();
        heightPixels.paramName = "heightPixels";
        heightPixels.paramValue = String.valueOf(dm.heightPixels);
        infoListItems.add(heightPixels);*/
        /**********************************************/

        /*Info widthPixels = new Info();
        widthPixels.paramName = "widthPixels";
        widthPixels.paramValue = String.valueOf(dm.widthPixels);
        infoListItems.add(widthPixels);*/

        /**********************************************/

        /*Info xdpi = new Info();
        xdpi.paramName = "xdpi";
        xdpi.paramValue = String.valueOf(dm.xdpi);
        infoListItems.add(xdpi);*/

        /**********************************************/

        /*Info ydpi = new Info();
        ydpi.paramName = "ydpi";
        ydpi.paramValue = String.valueOf(dm.ydpi);
        infoListItems.add(ydpi);*/

        /**********************************************/

        Info densityBucketDerived = new Info();
        densityBucketDerived.paramName = "Screen Density bucket";
        densityBucketDerived.paramValue = getDensityBucketDerived(dm);
        infoListItems.add(densityBucketDerived);

        /**********************************************/

        Info screenSize = new Info();
        screenSize.paramName = "Screen Size bucket";
        screenSize.paramValue = getScreenSize();
        infoListItems.add(screenSize);

        /**********************************************/
        Info orientation = new Info();
        orientation.paramName = "Orientation";
        orientation.paramValue = getScreenOrientation();
        infoListItems.add(orientation);

        /**********************************************/
        Info aspect = new Info();
        aspect.paramName = "Screen Aspect";
        aspect.paramValue = getScreenAspect(dm);
        infoListItems.add(aspect);

        /**********************************************/
        /*Info screenHeightdp = new Info();
        screenHeightdp.paramName = "Screen Height in dp";
        screenHeightdp.paramValue = getScreenHeightDp();
        infoListItems.add(screenHeightdp);*/

        /*********************************************/
        /*Info screenWidthdp = new Info();
        screenWidthdp.paramName = "Screen Width in dp";
        screenWidthdp.paramValue = getScreenWidthDp();
        infoListItems.add(screenWidthdp);*/

        /*********************************************/

        /*Info smallestScreenWidthdp = new Info();
        smallestScreenWidthdp.paramName = "Smallest Screen Width in dp";
        smallestScreenWidthdp.paramValue = getSmallestScreenWidthDp();
        infoListItems.add(smallestScreenWidthdp); */

        /*********************************************/

        /*Info deviceBoard = new Info();
        deviceBoard.paramName = "Device Board";
        deviceBoard.paramValue = getdeviceBoard();
        infoListItems.add(deviceBoard);*/

        /*********************************************/

        /*Info deviceBrand = new Info();
        deviceBrand.paramName = "Device Brand";
        deviceBrand.paramValue = getdeviceBrand();
        infoListItems.add(deviceBrand);*/

        /*********************************************/

        Info deviceDevice = new Info();
        deviceDevice.paramName = "Device";
        deviceDevice.paramValue = getDeviceDevice();
        infoListItems.add(deviceDevice);

        /*********************************************/

        /*Info deviceDisplay = new Info();
        deviceDisplay.paramName = "Display";
        deviceDisplay.paramValue = getdeviceDisplay();
        infoListItems.add(deviceDisplay);*/

        /*********************************************/

        /*Info deviceHardware = new Info();
        deviceHardware.paramName = "Hardware";
        deviceHardware.paramValue = getdeviceHardware();
        infoListItems.add(deviceHardware);*/

        /*********************************************/

        /*Info deviceID = new Info();
        deviceID.paramName = "ID";
        deviceID.paramValue = getdeviceID();
        infoListItems.add(deviceID);*/

        /*********************************************/

        Info deviceManufacturer = new Info();
        deviceManufacturer.paramName = "Manufacturer";
        deviceManufacturer.paramValue = getdeviceManufacturer();
        infoListItems.add(deviceManufacturer);

        /*********************************************/

        Info deviceModel = new Info();
        deviceModel.paramName = "Model";
        deviceModel.paramValue = getdeviceModel();
        infoListItems.add(deviceModel);

        /*********************************************/

        Info deviceProduct = new Info();
        deviceProduct.paramName = "Product";
        deviceProduct.paramValue = getdeviceProduct();
        infoListItems.add(deviceProduct);

        /*********************************************/

        /*Info deviceType = new Info();
        deviceType.paramName = "Type";
        deviceType.paramValue = getdeviceType();
        infoListItems.add(deviceType);*/

        /*********************************************/

        /*Info deviceUser = new Info();
        deviceUser.paramName = "User";
        deviceUser.paramValue = getdeviceUser();
        infoListItems.add(deviceUser);*/

        /*********************************************/

        Info deviceRelease = new Info();
        deviceRelease.paramName = "Release";
        deviceRelease.paramValue = getdeviceRelease();
        infoListItems.add(deviceRelease);

        /*********************************************/

        Info deviceSDKVersion = new Info();
        deviceSDKVersion.paramName = "SDK Version";
        deviceSDKVersion.paramValue = getdeviceSDKInt();
        infoListItems.add(deviceSDKVersion);

        /*********************************************/

        /*Info deviceFingerprint = new Info();
        deviceFingerprint.paramName = "F";
        deviceFingerprint.paramValue = getdeviceFingerprint();
        infoListItems.add(deviceFingerprint);*/


        /*Getting camera properties.*/
        Info hasCamera = new Info();
        hasCamera.paramName = "Has Camera";
        boolean hasCamera2 = checkCameraHardware(this);
        hasCamera.paramValue = String.valueOf(hasCamera2);
        infoListItems.add(hasCamera);

        if(hasCamera2) {
            Info cameraAvailable = new Info();
            Camera c = getCameraInstance();
            cameraAvailable.paramName = "Camera Available";
            cameraAvailable.paramValue = c == null ? "No" : "Yes : " + String.valueOf(c.getNumberOfCameras());
            infoListItems.add(cameraAvailable);

            if(c!=null) {
                Camera.Parameters params = c.getParameters();

                Info autoFocusSupported = new Info();
                autoFocusSupported.paramName = "Auto focus supported";
                List<String> supportedFocusModes = params.getSupportedFocusModes();
                boolean hasAutoFocus = supportedFocusModes != null && supportedFocusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO);
                autoFocusSupported.paramValue = String.valueOf(hasAutoFocus);
                infoListItems.add(autoFocusSupported);

                Info isAutoExposureSupported = new Info();
                isAutoExposureSupported.paramName = "Auto Exposure supported";
                isAutoExposureSupported.paramValue = String.valueOf(params.isAutoExposureLockSupported());
                infoListItems.add(isAutoExposureSupported);

                Info focalLength = new Info();
                focalLength.paramName = "Focal length";
                focalLength.paramValue = String.valueOf(params.getFocalLength());
                infoListItems.add(focalLength);

                Info opticalStablization = new Info();
                opticalStablization.paramName = "Optical stablization";
                opticalStablization.paramValue = String.valueOf(params.isVideoStabilizationSupported());
                infoListItems.add(opticalStablization);

                Info resolution = new Info();
                resolution.paramName = "Resolution";
                List<Camera.Size> sizes = params.getSupportedPictureSizes();
                if(sizes != null){
                    long pixelCount = -1;
                    for(Camera.Size s: sizes){
                        long tempSize = s.height * s.width;
                        if(pixelCount < tempSize){
                            pixelCount = tempSize;
                        }
                    }
                    resolution.paramValue = String.valueOf(((float)pixelCount) / (1024000.0f));
                }else{
                    resolution.paramValue = "--";
                }
                infoListItems.add(resolution);

                c.release();
            }
        }
    }

    /*Camera functions*/
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {

            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public void getCameraProperties(Camera.Parameters params){

    }


    private String getScreenOrientation() {
        switch (getResources().getConfiguration().orientation) {
            case 1:
                return "Portrait";
            case 2:
                return "Landscape";
        }
        return "undefined";
    }

    private String getScreenAspect(DisplayMetrics metrics) {
        int screenLayout = getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_LONG_MASK;
        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_LONG_NO:
                return "notlong";
            case Configuration.SCREENLAYOUT_LONG_YES:
                return "long";
            case Configuration.SCREENLAYOUT_LONG_UNDEFINED:
                return "undefined";
        }

        return "undefined";
    }

    private String getScreenSize() {
        int screenLayout = getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return "small";
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return "normal";
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return "large";
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                return "xlarge";
        }

        return "undefined";
    }

    private String getDensityBucketDerived(DisplayMetrics metrics) {

        switch (metrics.densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                return getString(R.string.label_density_low);

            case DisplayMetrics.DENSITY_MEDIUM:
                return getString(R.string.label_density_medium);

            case DisplayMetrics.DENSITY_TV:
                return getString(R.string.label_density_tv);

            case DisplayMetrics.DENSITY_HIGH:
                return getString(R.string.label_density_high);

            case DisplayMetrics.DENSITY_XHIGH:
                return getString(R.string.label_density_xhigh);

            case DisplayMetrics.DENSITY_400:
                return getString(R.string.label_density_400);

            case DisplayMetrics.DENSITY_XXHIGH:
                return getString(R.string.label_density_xxhigh);

            case DisplayMetrics.DENSITY_560:
                return getString(R.string.label_density_560);

            case DisplayMetrics.DENSITY_XXXHIGH:
                return getString(R.string.label_density_xxxhigh);

            default:
                return getString(R.string.label_density_not_specified) + metrics.densityDpi;
        }
    }

    private String getScreenHeightDp() {

        int screenHeightDp = getResources().getConfiguration().screenHeightDp;

        return Integer.toString(screenHeightDp);

        //need to add cases where this value might be un-defined

    }

    private String getScreenWidthDp() {

        int screenWidthDp = getResources().getConfiguration().screenWidthDp;

        return Integer.toString(screenWidthDp);

        //need to add cases where this value might be un-defined

    }

    private String getSmallestScreenWidthDp() {

        int smallestScreenWidthDp = getResources().getConfiguration().smallestScreenWidthDp;

        return Integer.toString(smallestScreenWidthDp);

        //need to add cases where this value might be un-defined

    }

    private String getdeviceBoard() {

        String deviceBoard = Build.BRAND;

        return deviceBoard;

        //need to add cases where this value might be un-defined

    }

    private String getdeviceBrand() {

        String deviceBrand = Build.BRAND;

        return deviceBrand;

        //need to add cases where this value might be un-defined

    }

    private String getDevice() {

        String device = Build.DEVICE;

        return device;

        //need to add cases where this value might be un-defined

    }

    private String getDeviceDevice() {

        String deviceName = Build.DEVICE;

        return deviceName;
    }

    private String getdeviceDisplay() {

        String deviceDisplay = Build.DISPLAY;

        return deviceDisplay;
    }

    private String getdeviceHardware() {

        String deviceHardware = Build.HARDWARE;

        return deviceHardware;
    }

    private String getdeviceID() {

        String deviceID = Build.ID;

        return deviceID;
    }

    private String getdeviceManufacturer() {

        String deviceManufacturer = Build.MANUFACTURER;

        return deviceManufacturer;
    }

    private String getdeviceModel() {

        String deviceModel = Build.MODEL;

        return deviceModel;
    }

    private String getdeviceProduct() {

        String deviceProduct = Build.PRODUCT;

        return deviceProduct;
    }

    private String getdeviceType() {

        String deviceType = Build.TYPE;

        return deviceType;
    }

    private String getdeviceUser() {

        String deviceType = Build.TYPE;

        return deviceType;
    }

    private String getdeviceRelease() {

        String deviceRelease = Build.VERSION.RELEASE;

        return deviceRelease;
    }


    private String getdeviceSDKInt() {

        String deviceSDKInt = Integer.toString(Build.VERSION.SDK_INT);

        return deviceSDKInt;

        /*switch(deviceSDKInt) {

            case Build.VERSION_CODES.BASE:
                return "October 2008: The original, first, version of Android. Yay!";

            case Build.VERSION_CODES.BASE_1_1:
                return "February 2009: First Android update, officially called 1.1";

            case Build.VERSION_CODES.CUPCAKE:
                return "May 2009: Android 1.5";

            case Build.VERSION_CODES.DONUT:
                return "September 2009: Android 1.6";

            case Build.VERSION_CODES.ECLAIR:
                return "November 2009: Android 2.0";

            case Build.VERSION_CODES.ECLAIR_0_1:
                return "December 2009: Android 2.0.1";

            case Build.VERSION_CODES.ECLAIR_MR1:
                return "January 2010: Android 2.1";

            case Build.VERSION_CODES.FROYO:
                return "June 2010: Android 2.2";

            case Build.VERSION_CODES.GINGERBREAD:
                return "November 2010: Android 2.3";

            case Build.VERSION_CODES.GINGERBREAD_MR1:
                return "February 2011: Android 2.3.3";

            case Build.VERSION_CODES.HONEYCOMB:
                return "February 2011: Android 3.0";

            case Build.VERSION_CODES.HONEYCOMB_MR1:
                return "May 2011: Android 3.1.";

            case Build.VERSION_CODES.HONEYCOMB_MR2:
                return "June 2011: Android 3.2.";

            case Build.VERSION_CODES.ICE_CREAM_SANDWICH:
                return "October 2011: Android 4.0";

            case Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1:
                return "December 2011: Android 4.0.3.";

            case Build.VERSION_CODES.JELLY_BEAN:
                return "June 2012: Android 4.1";

            case Build.VERSION_CODES.JELLY_BEAN_MR1:
                return "November 2012: Android 4.2, Moar jelly beans!";

            case Build.VERSION_CODES.JELLY_BEAN_MR2:
                return "July 2013: Android 4.3, the revenge of the beans.";

            case Build.VERSION_CODES.KITKAT:
                return "October 2013: Android 4.4, KitKat, another tasty treat.";

            case Build.VERSION_CODES.KITKAT_WATCH:
                return "Android 4.4W: KitKat for watches, snacks on the run.";

            case Build.VERSION_CODES.LOLLIPOP:
                return "Lollipop. A flat one with beautiful shadows. But still tasty.";

            case Build.VERSION_CODES.LOLLIPOP_MR1:
                return "Lollipop with an extra sugar coating on the outside!";

            default:
                return Integer.toString(Build.VERSION.SDK_INT);
        }*/

    }

    private String getdeviceFingerprint() {

        String deviceRelease = Build.FINGERPRINT;

        return deviceRelease;
    }

    public void getCpuInfo() {
        try {
            Process proc = Runtime.getRuntime().exec("cat /proc/cpuinfo");
            InputStream is = proc.getInputStream();
            Info cpuInfo = new Info();
            cpuInfo.paramName = "CPU";
            cpuInfo.paramValue = getStringFromInputStream(is);
            adapter.getInfoList().add(cpuInfo);
        } catch (IOException e) {
            Log.e(TAG, "------ getCpuInfo " + e.getMessage());
        }
    }

    public void getMemoryInfo() {
        try {
            Process proc = Runtime.getRuntime().exec("cat /proc/meminfo");
            InputStream is = proc.getInputStream();
            Info memInfo = new Info();
            memInfo.paramName = "CPU";
            memInfo.paramValue = getStringFromInputStream(is);
            adapter.getInfoList().add(memInfo);
        } catch (IOException e) {
            Log.e(TAG, "------ getMemoryInfo " + e.getMessage());
        }
    }

    private static String getStringFromInputStream(InputStream is) {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = null;

        try {
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            Log.e(TAG, "------ getStringFromInputStream " + e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Log.e(TAG, "------ getStringFromInputStream " + e.getMessage());
                }
            }
        }

        return sb.toString();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private class Info {
        String paramName;
        String paramValue;
    }


    private class InfoListAdapter extends ArrayAdapter<Info> {
        private List<Info> infoItems;

        private int mListItemLayoutResId;

        public InfoListAdapter(Context context, List<Info> items) {
            this(context, android.R.layout.two_line_list_item, items);
        }

        public InfoListAdapter(Context context, int resource, List<Info> objects) {
            super(context, resource, objects);
            mListItemLayoutResId = resource;
            infoItems = objects;
        }

        @Override
        public int getCount() {
            return infoItems.size();
        }

        public List<Info> getInfoList() {
            return infoItems;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View listItemView = convertView;
            if (null == convertView) {
                listItemView = inflater.inflate(mListItemLayoutResId, parent, false);
            }
            TextView lineOneView = (TextView) listItemView.findViewById(android.R.id.text1);
            TextView lineTwoView = (TextView) listItemView.findViewById(android.R.id.text2);
            Info i = getItem(position);
            lineOneView.setText(i.paramName);
            lineTwoView.setText(i.paramValue);

            return listItemView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
